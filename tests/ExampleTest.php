<?php

class ExampleTest extends PHPUnit_Framework_TestCase
{

    /**
     * @group failing
     * Tests the api edit form
     */
    public function testGreetings()
    {
        $greetings = 'Hello World';
        $this->assertEquals('Hello World', $greetings);
    }

    /**
     * @group failing
     * Tests the api edit form
     */
    public function testGreetingsCopy()
    {
        $greetings = 'Hello World';
        $this->assertEquals('Hello World', $greetings);
    }

    /**
     * @group zzbbzbz
     * Tests the api edit form
     */
    public function testGreetingsC()
    {
        $greetings = 'Hello';
        $this->assertEquals('Hello World', $greetings);
    }

}
